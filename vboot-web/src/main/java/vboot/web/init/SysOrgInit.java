package vboot.web.init;

import cn.hutool.core.util.StrUtil;
import vboot.core.module.sys.org.dept.SysOrgDept;
import vboot.core.module.sys.org.dept.SysOrgDeptService;
import vboot.core.module.sys.org.group.SysOrgGroup;
import vboot.core.module.sys.org.group.SysOrgGroupCate;
import vboot.core.module.sys.org.group.SysOrgGroupCateService;
import vboot.core.module.sys.org.group.SysOrgGroupService;
import vboot.core.module.sys.org.post.SysOrgPost;
import vboot.core.module.sys.org.post.SysOrgPostService;
import vboot.core.module.sys.org.role.*;
import vboot.core.module.sys.org.root.SysOrg;
import vboot.core.module.sys.org.user.SysOrgUser;
import vboot.core.module.sys.org.user.SysOrgUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

//组织架构初始化，可根据application.properties的app.init.orgType配置生成不同的基础数据
@Component
public class SysOrgInit {

    private String rootId = "1";

    //初始化部门
    protected void initWcDept() {
        List<SysOrgDept> deptList = new ArrayList<>();

        SysOrgDept a=new SysOrgDept();
        a.setId("a");
        a.setName("A组_卡厄塞荷");
        a.setLabel("1");
        a.setOrnum(1);
        a.setAvtag(true);
        a.setTier("x"+rootId+"xax");
        a.setType(1);
        a.setParent(new SysOrg(rootId));
        deptList.add(a);

        SysOrgDept a1=new SysOrgDept();
        a1.setId("a1");
        a1.setName("卡塔尔");
        a1.setLabel("1");
        a1.setOrnum(1);
        a1.setAvtag(true);
        a1.setTier("x"+rootId+"xaxa1x");
        a1.setType(1);
        a1.setParent(new SysOrg("a"));
        deptList.add(a1);
        addPostionDept(deptList,"a1");

        SysOrgDept a2=new SysOrgDept();
        a2.setId("a2");
        a2.setName("厄瓜多尔");
        a2.setLabel("1");
        a2.setOrnum(2);
        a2.setAvtag(true);
        a2.setTier("x"+rootId+"xaxa2x");
        a2.setType(1);
        a2.setParent(new SysOrg("a"));
        deptList.add(a2);
        addPostionDept(deptList,"a2");

        SysOrgDept a3=new SysOrgDept();
        a3.setId("a3");
        a3.setName("塞内加尔");
        a3.setLabel("1");
        a3.setOrnum(3);
        a3.setAvtag(true);
        a3.setTier("x"+rootId+"xaxa3x");
        a3.setType(1);
        a3.setParent(new SysOrg("a"));
        deptList.add(a3);
        addPostionDept(deptList,"a3");

        SysOrgDept a4=new SysOrgDept();
        a4.setId("a4");
        a4.setName("荷兰");
        a4.setLabel("1");
        a4.setOrnum(4);
        a4.setAvtag(true);
        a4.setTier("x"+rootId+"xaxa4x");
        a4.setType(1);
        a4.setParent(new SysOrg("a"));
        deptList.add(a4);
        addPostionDept(deptList,"a4");

        SysOrgDept b=new SysOrgDept();
        b.setId("b");
        b.setName("B组_英伊美威");
        b.setLabel("1");
        b.setOrnum(2);
        b.setAvtag(true);
        b.setTier("x"+rootId+"xbx");
        b.setType(1);
        b.setParent(new SysOrg(rootId));
        deptList.add(b);

        SysOrgDept b1=new SysOrgDept();
        b1.setId("b1");
        b1.setName("英格兰");
        b1.setLabel("1");
        b1.setOrnum(1);
        b1.setAvtag(true);
        b1.setTier("x"+rootId+"xbxb1x");
        b1.setType(1);
        b1.setParent(new SysOrg("b"));
        deptList.add(b1);
        addPostionDept(deptList,"b1");

        SysOrgDept b2=new SysOrgDept();
        b2.setId("b2");
        b2.setName("伊朗");
        b2.setLabel("1");
        b2.setOrnum(2);
        b2.setAvtag(true);
        b2.setTier("x"+rootId+"xbxb2x");
        b2.setType(1);
        b2.setParent(new SysOrg("b"));
        deptList.add(b2);
        addPostionDept(deptList,"b2");

        SysOrgDept b3=new SysOrgDept();
        b3.setId("b3");
        b3.setName("美国");
        b3.setLabel("1");
        b3.setOrnum(3);
        b3.setAvtag(true);
        b3.setTier("x"+rootId+"xbxb3x");
        b3.setType(1);
        b3.setParent(new SysOrg("b"));
        deptList.add(b3);
        addPostionDept(deptList,"b3");

        SysOrgDept b4=new SysOrgDept();
        b4.setId("b4");
        b4.setName("威尔士");
        b4.setLabel("1");
        b4.setOrnum(4);
        b4.setAvtag(true);
        b4.setTier("x"+rootId+"xbxb4x");
        b4.setType(1);
        b4.setParent(new SysOrg("b"));
        deptList.add(b4);
        addPostionDept(deptList,"b4");

        SysOrgDept c=new SysOrgDept();
        c.setId("c");
        c.setName("C组_阿沙墨波");
        c.setLabel("1");
        c.setOrnum(3);
        c.setAvtag(true);
        c.setTier("x"+rootId+"xcx");
        c.setType(1);
        c.setParent(new SysOrg(rootId));
        deptList.add(c);

        SysOrgDept c1=new SysOrgDept();
        c1.setId("c1");
        c1.setName("阿根廷");
        c1.setLabel("1");
        c1.setOrnum(1);
        c1.setAvtag(true);
        c1.setTier("x"+rootId+"xcxc1x");
        c1.setType(1);
        c1.setParent(new SysOrg("c"));
        deptList.add(c1);
        addPostionDept(deptList,"c1");

        SysOrgDept c2=new SysOrgDept();
        c2.setId("c22");
        c2.setName("沙特");
        c2.setLabel("1");
        c2.setOrnum(2);
        c2.setAvtag(true);
        c2.setTier("x"+rootId+"xcxc22x");
        c2.setType(1);
        c2.setParent(new SysOrg("c"));
        deptList.add(c2);
        addPostionDept(deptList,"c22");

        SysOrgDept c3=new SysOrgDept();
        c3.setId("c3");
        c3.setName("墨西哥");
        c3.setLabel("1");
        c3.setOrnum(3);
        c3.setAvtag(true);
        c3.setTier("x"+rootId+"xcxc3x");
        c3.setType(1);
        c3.setParent(new SysOrg("c"));
        deptList.add(c3);
        addPostionDept(deptList,"c3");

        SysOrgDept c4=new SysOrgDept();
        c4.setId("c4");
        c4.setName("波兰");
        c4.setLabel("1");
        c4.setOrnum(4);
        c4.setAvtag(true);
        c4.setTier("x"+rootId+"xcxc4x");
        c4.setType(1);
        c4.setParent(new SysOrg("c"));
        deptList.add(c4);
        addPostionDept(deptList,"c4");

        SysOrgDept d=new SysOrgDept();
        d.setId("d");
        d.setName("D组_法澳丹突");
        d.setLabel("1");
        d.setOrnum(4);
        d.setAvtag(true);
        d.setTier("x"+rootId+"xdx");
        d.setType(1);
        d.setParent(new SysOrg(rootId));
        deptList.add(d);

        SysOrgDept d1=new SysOrgDept();
        d1.setId("d1");
        d1.setName("法国");
        d1.setLabel("1");
        d1.setOrnum(1);
        d1.setAvtag(true);
        d1.setTier("x"+rootId+"xdxd1x");
        d1.setType(1);
        d1.setParent(new SysOrg("d"));
        deptList.add(d1);
        addPostionDept(deptList,"d1");

        SysOrgDept d2=new SysOrgDept();
        d2.setId("d2");
        d2.setName("澳大利亚");
        d2.setLabel("1");
        d2.setOrnum(2);
        d2.setAvtag(true);
        d2.setTier("x"+rootId+"xdxd2x");
        d2.setType(1);
        d2.setParent(new SysOrg("d"));
        deptList.add(d2);
        addPostionDept(deptList,"d2");

        SysOrgDept d3=new SysOrgDept();
        d3.setId("d3");
        d3.setName("丹麦");
        d3.setLabel("1");
        d3.setOrnum(3);
        d3.setAvtag(true);
        d3.setTier("x"+rootId+"xdxd3x");
        d3.setType(1);
        d3.setParent(new SysOrg("d"));
        deptList.add(d3);
        addPostionDept(deptList,"d3");

        SysOrgDept d4=new SysOrgDept();
        d4.setId("d4");
        d4.setName("突尼斯");
        d4.setLabel("1");
        d4.setOrnum(4);
        d4.setAvtag(true);
        d4.setTier("x"+rootId+"xdxd4x");
        d4.setType(1);
        d4.setParent(new SysOrg("d"));
        deptList.add(d4);
        addPostionDept(deptList,"d4");

        SysOrgDept e=new SysOrgDept();
        e.setId("e");
        e.setName("E组_西哥德日");
        e.setLabel("1");
        e.setOrnum(5);
        e.setAvtag(true);
        e.setTier("x"+rootId+"xex");
        e.setType(1);
        e.setParent(new SysOrg(rootId));
        deptList.add(e);

        SysOrgDept e1=new SysOrgDept();
        e1.setId("e1");
        e1.setName("西班牙");
        e1.setLabel("1");
        e1.setOrnum(1);
        e1.setAvtag(true);
        e1.setTier("x"+rootId+"xexe1x");
        e1.setType(1);
        e1.setParent(new SysOrg("e"));
        deptList.add(e1);
        addPostionDept(deptList,"e1");

        SysOrgDept e2=new SysOrgDept();
        e2.setId("e2");
        e2.setName("哥斯达黎加");
        e2.setLabel("1");
        e2.setOrnum(2);
        e2.setAvtag(true);
        e2.setTier("x"+rootId+"xexe2x");
        e2.setType(1);
        e2.setParent(new SysOrg("e"));
        deptList.add(e2);
        addPostionDept(deptList,"e2");

        SysOrgDept e3=new SysOrgDept();
        e3.setId("e3");
        e3.setName("德国");
        e3.setLabel("1");
        e3.setOrnum(3);
        e3.setAvtag(true);
        e3.setTier("x"+rootId+"xexe3x");
        e3.setType(1);
        e3.setParent(new SysOrg("e"));
        deptList.add(e3);
        addPostionDept(deptList,"e3");

        SysOrgDept e4=new SysOrgDept();
        e4.setId("e4");
        e4.setName("日本");
        e4.setLabel("1");
        e4.setOrnum(4);
        e4.setAvtag(true);
        e4.setTier("x"+rootId+"xexe4x");
        e4.setType(1);
        e4.setParent(new SysOrg("e"));
        deptList.add(e4);
        addPostionDept(deptList,"e4");

        SysOrgDept f=new SysOrgDept();
        f.setId("f");
        f.setName("F组_比加摩克");
        f.setLabel("1");
        f.setOrnum(6);
        f.setAvtag(true);
        f.setTier("x"+rootId+"xfx");
        f.setType(1);
        f.setParent(new SysOrg(rootId));
        deptList.add(f);

        SysOrgDept f1=new SysOrgDept();
        f1.setId("f1");
        f1.setName("比利时");
        f1.setLabel("1");
        f1.setOrnum(1);
        f1.setAvtag(true);
        f1.setTier("x"+rootId+"xfxf1x");
        f1.setType(1);
        f1.setParent(new SysOrg("f"));
        deptList.add(f1);
        addPostionDept(deptList,"f1");

        SysOrgDept f2=new SysOrgDept();
        f2.setId("f2");
        f2.setName("加拿大");
        f2.setLabel("1");
        f2.setOrnum(2);
        f2.setAvtag(true);
        f2.setTier("x"+rootId+"xfxf2x");
        f2.setType(1);
        f2.setParent(new SysOrg("f"));
        deptList.add(f2);
        addPostionDept(deptList,"f2");

        SysOrgDept f3=new SysOrgDept();
        f3.setId("f3");
        f3.setName("摩洛哥");
        f3.setLabel("1");
        f3.setOrnum(3);
        f3.setAvtag(true);
        f3.setTier("x"+rootId+"xfxf3x");
        f3.setType(1);
        f3.setParent(new SysOrg("f"));
        deptList.add(f3);
        addPostionDept(deptList,"f3");

        SysOrgDept f4=new SysOrgDept();
        f4.setId("f4");
        f4.setName("克罗地亚");
        f4.setLabel("1");
        f4.setOrnum(4);
        f4.setAvtag(true);
        f4.setTier("x"+rootId+"xfxf4x");
        f4.setType(1);
        f4.setParent(new SysOrg("f"));
        deptList.add(f4);
        addPostionDept(deptList,"f4");

        SysOrgDept g=new SysOrgDept();
        g.setId("g");
        g.setName("G组_巴塞瑞喀");
        g.setLabel("1");
        g.setOrnum(7);
        g.setAvtag(true);
        g.setTier("x"+rootId+"xgx");
        g.setType(1);
        g.setParent(new SysOrg(rootId));
        deptList.add(g);

        SysOrgDept g1=new SysOrgDept();
        g1.setId("g1");
        g1.setName("巴西");
        g1.setLabel("1");
        g1.setOrnum(1);
        g1.setAvtag(true);
        g1.setTier("x"+rootId+"xgxg1x");
        g1.setType(1);
        g1.setParent(new SysOrg("g"));
        deptList.add(g1);
        addPostionDept(deptList,"g1");

        SysOrgDept g2=new SysOrgDept();
        g2.setId("g2");
        g2.setName("塞尔维亚");
        g2.setLabel("1");
        g2.setOrnum(2);
        g2.setAvtag(true);
        g2.setTier("x"+rootId+"xgxg2x");
        g2.setType(1);
        g2.setParent(new SysOrg("g"));
        deptList.add(g2);
        addPostionDept(deptList,"g2");

        SysOrgDept g3=new SysOrgDept();
        g3.setId("g3");
        g3.setName("瑞士");
        g3.setLabel("1");
        g3.setOrnum(3);
        g3.setAvtag(true);
        g3.setTier("x"+rootId+"xgxg3x");
        g3.setType(1);
        g3.setParent(new SysOrg("g"));
        deptList.add(g3);
        addPostionDept(deptList,"g3");

        SysOrgDept g4=new SysOrgDept();
        g4.setId("g4");
        g4.setName("喀麦隆");
        g4.setLabel("1");
        g4.setOrnum(4);
        g4.setAvtag(true);
        g4.setTier("x"+rootId+"xgxg4x");
        g4.setType(1);
        g4.setParent(new SysOrg("g"));
        deptList.add(g4);
        addPostionDept(deptList,"g4");

        SysOrgDept h=new SysOrgDept();
        h.setId("h");
        h.setName("H组_葡加乌韩");
        h.setLabel("1");
        h.setOrnum(8);
        h.setAvtag(true);
        h.setTier("x"+rootId+"xhx");
        h.setType(1);
        h.setParent(new SysOrg(rootId));
        deptList.add(h);

        SysOrgDept h1=new SysOrgDept();
        h1.setId("h1");
        h1.setName("葡萄牙");
        h1.setLabel("1");
        h1.setOrnum(1);
        h1.setAvtag(true);
        h1.setTier("x"+rootId+"xhxh1x");
        h1.setType(1);
        h1.setParent(new SysOrg("h"));
        deptList.add(h1);
        addPostionDept(deptList,"h1");

        SysOrgDept h2=new SysOrgDept();
        h2.setId("h2");
        h2.setName("加纳");
        h2.setLabel("1");
        h2.setOrnum(2);
        h2.setAvtag(true);
        h2.setTier("x"+rootId+"xhxh2x");
        h2.setType(1);
        h2.setParent(new SysOrg("h"));
        deptList.add(h2);
        addPostionDept(deptList,"h2");

        SysOrgDept h3=new SysOrgDept();
        h3.setId("h3");
        h3.setName("乌拉圭");
        h3.setLabel("1");
        h3.setOrnum(3);
        h3.setAvtag(true);
        h3.setTier("x"+rootId+"xhxh3x");
        h3.setType(1);
        h3.setParent(new SysOrg("h"));
        deptList.add(h3);
        addPostionDept(deptList,"h3");

        SysOrgDept h4=new SysOrgDept();
        h4.setId("h4");
        h4.setName("韩国");
        h4.setLabel("1");
        h4.setOrnum(4);
        h4.setAvtag(true);
        h4.setTier("x"+rootId+"xhxh4x");
        h4.setType(1);
        h4.setParent(new SysOrg("h"));
        deptList.add(h4);
        addPostionDept(deptList,"h4");

//        SysOrgDept ec1=new SysOrgDept();
//        ec1.setId("ec1");
//        ec1.setName("客户");
//        ec1.setLabel("ec1");
//        ec1.setOrnum(1);
//        ec1.setAvtag(true);
//        ec1.setTier("xec1x");
//        deptList.add(ec1);
//
//        SysOrgDept ec2=new SysOrgDept();
//        ec2.setId("ec2");
//        ec2.setName("渠道商");
//        ec2.setLabel("ec2");
//        ec2.setOrnum(2);
//        ec2.setAvtag(true);
//        ec2.setTier("xec2x");
//        deptList.add(ec2);
//
//        SysOrgDept ec3=new SysOrgDept();
//        ec3.setId("ec3");
//        ec3.setName("供应商");
//        ec3.setLabel("ec3");
//        ec3.setOrnum(3);
//        ec3.setAvtag(true);
//        ec3.setTier("xec3x");
//        deptList.add(ec3);

        deptService.insertAll(deptList);
    }

    protected void initRoot(String name,String id) {
        if(StrUtil.isNotBlank(id)){
            rootId=id;
        }
        SysOrgDept root = new SysOrgDept();
        root.setId(rootId);
        root.setName(name);
        root.setLabel("1");
        root.setOrnum(0);
        root.setAvtag(true);
        root.setTier("x"+rootId+"x");
        root.setType(1);
        deptService.insert(root);
    }

    //初始化管理员
    protected void initSa() {
        SysOrgUser user = new SysOrgUser();
        user.setId("sa");
        user.setUsnam("sa");
        user.setName("管理员");
        user.setPacod("1");
        user.setAvtag(true);
        user.setDept(new SysOrg(rootId));
        user.setTier("x"+rootId+"xsax");
        userService.insert(user);

        SysOrgUser user2 = new SysOrgUser();
        user2.setId("vben");
        user2.setUsnam("vben");
        user2.setName("小维");
        user2.setPacod("123456");
        user2.setAvtag(true);
        user2.setDept(new SysOrg(rootId));
        user2.setTier("x"+rootId+"xvbenx");
        userService.insert(user2);
    }

    //初始化测试部门与人员
    protected void initTestOrg() {
        SysOrgDept d1=new SysOrgDept();
        d1.setId("dept1");
        d1.setName("测试一部");
        d1.setLabel("1");
        d1.setOrnum(9991);
        d1.setAvtag(true);
        d1.setType(2);
        d1.setParent(new SysOrg(rootId));
        d1.setTier("x"+rootId+"xdept1x");
        deptService.insert(d1);

        SysOrgDept d2=new SysOrgDept();
        d2.setId("dept2");
        d2.setName("测试二部");
        d2.setLabel("1");
        d2.setOrnum(9992);
        d2.setAvtag(true);
        d2.setType(2);
        d2.setParent(new SysOrg(rootId));
        d2.setTier("x"+rootId+"xdept2x");
        deptService.insert(d2);

        List<SysOrgUser> userList=new ArrayList<>();
        SysOrgUser u1 = new SysOrgUser();
        u1.setId("l1");
        u1.setUsnam("l1");
        u1.setName("刘一");
        u1.setPacod("1");
        u1.setOrnum(1);
        u1.setAvtag(true);
        u1.setDept(new SysOrg("dept1"));
        u1.setTier("x"+rootId+"xdept1xl1x");
        userList.add(u1);

        SysOrgUser u2 = new SysOrgUser();
        u2.setId("c2");
        u2.setUsnam("c2");
        u2.setName("陈二");
        u2.setPacod("1");
        u2.setOrnum(2);
        u2.setAvtag(true);
        u2.setDept(new SysOrg("dept1"));
        u2.setTier("x"+rootId+"xdept1xc2x");
        userList.add(u2);

        SysOrgUser u3 = new SysOrgUser();
        u3.setId("z3");
        u3.setUsnam("z3");
        u3.setName("张三");
        u3.setPacod("1");
        u3.setOrnum(3);
        u3.setAvtag(true);
        u3.setDept(new SysOrg("dept1"));
        u3.setTier("x"+rootId+"xdept1xz3x");
        userList.add(u3);

        SysOrgUser u4 = new SysOrgUser();
        u4.setId("l4");
        u4.setUsnam("l4");
        u4.setName("李四");
        u4.setPacod("1");
        u4.setOrnum(4);
        u4.setAvtag(true);
        u4.setDept(new SysOrg("dept1"));
        u4.setTier("x"+rootId+"xdept1xl4x");
        userList.add(u4);

        SysOrgUser u5 = new SysOrgUser();
        u5.setId("w5");
        u5.setUsnam("w5");
        u5.setName("王五");
        u5.setPacod("1");
        u5.setOrnum(5);
        u5.setAvtag(true);
        u5.setDept(new SysOrg("dept2"));
        u5.setTier("x"+rootId+"xdept2xw5x");
        userList.add(u5);

        SysOrgUser u6 = new SysOrgUser();
        u6.setId("z6");
        u6.setUsnam("z6");
        u6.setName("赵六");
        u6.setPacod("1");
        u6.setOrnum(6);
        u6.setAvtag(true);
        u6.setDept(new SysOrg("dept2"));
        u6.setTier("x"+rootId+"xdept2xz6x");
        userList.add(u6);

        SysOrgUser u7 = new SysOrgUser();
        u7.setId("s7");
        u7.setUsnam("s7");
        u7.setName("孙七");
        u7.setPacod("1");
        u7.setOrnum(7);
        u7.setAvtag(true);
        u7.setDept(new SysOrg("dept2"));
        u7.setTier("x"+rootId+"xdept2xs7x");
        userList.add(u7);

        SysOrgUser u8 = new SysOrgUser();
        u8.setId("z8");
        u8.setUsnam("z8");
        u8.setName("周八");
        u8.setPacod("1");
        u8.setOrnum(8);
        u8.setAvtag(true);
        u8.setDept(new SysOrg("dept2"));
        u8.setTier("x"+rootId+"xdept2xz8x");
        userList.add(u8);

        userService.insertAll(userList);

        SysOrgPost p1 = new SysOrgPost();
        p1.setId("post1");
        p1.setName("一部负责人");
        p1.setOrnum(0);
        p1.setAvtag(true);
        p1.setDept(new SysOrg("dept1"));
        p1.setTier("x"+rootId+"xdept1xpost1x");
        p1.setUsers(new ArrayList<>());
        p1.getUsers().add(new SysOrg("l1","刘一",8));
        postService.insert(p1);

        SysOrgPost p2 = new SysOrgPost();
        p2.setId("post2");
        p2.setName("二部负责人");
        p2.setOrnum(0);
        p2.setAvtag(true);
        p2.setDept(new SysOrg("dept2"));
        p2.setTier("x"+rootId+"xdept2xpost2x");
        p2.setUsers(new ArrayList<>());
        p2.getUsers().add(new SysOrg("w5","王五",8));
        p2.getUsers().add(new SysOrg("z6","赵六",8));
        postService.insert(p2);

        SysOrgGroupCate gcate = new SysOrgGroupCate();
        gcate.setId("gcate");
        gcate.setName("测试群组分类");
        gcate.setAvtag(true);
        gcate.setOrnum(1);
        gcate.setCrman(new SysOrg("sa"));
        groupCateService.insert(gcate);

        SysOrgGroup g1 = new SysOrgGroup();
        g1.setId("group1");
        g1.setName("张三李四组");
        g1.setOrnum(1);
        g1.setAvtag(true);
        g1.setCatid("gcate");
        g1.setMembers(new ArrayList<>());
        g1.getMembers().add(new SysOrg("z3","张三",8));
        g1.getMembers().add(new SysOrg("l4","李四",8));
        groupService.insert(g1);

        SysOrgGroup g2 = new SysOrgGroup();
        g2.setId("group2");
        g2.setName("用户岗位部门混合组");
        g2.setOrnum(2);
        g2.setAvtag(true);
        g2.setCatid("gcate");
        g2.setMembers(new ArrayList<>());
        g2.getMembers().add(new SysOrg("c2","陈二",8));
        g2.getMembers().add(new SysOrg("post1","一部负责人",4));
        g2.getMembers().add(new SysOrg("dept2","测试二部",2));
        groupService.insert(g2);

        SysOrgRoleTree rtree1 = new SysOrgRoleTree();
        rtree1.setId("rtree1");
        rtree1.setName("业务角色树");
        rtree1.setOrnum(1);
        rtree1.setAvtag(true);
        rtree1.setRoles(new ArrayList<>());
        rtree1.getRoles().add(new SysOrgRole("role1","分管领导",1));
        rtree1.getRoles().add(new SysOrgRole("role2","部门领导",2));
        rtree1.setCrman(new SysOrg("sa"));
        roleTreeService.insertx(rtree1);

        SysOrgRoleNode rnode1 = new SysOrgRoleNode();
        rnode1.setId("rnode1");
        rnode1.setName("一部分管领导");
        rnode1.setTreid("rtree1");
        rnode1.setOrnum(1);
        rnode1.setAvtag(true);
        rnode1.setMember(new SysOrg("post1"));
        rnode1.setTier("xrnode1x");
        roleNodeService.insert(rnode1);

        SysOrgRoleNode rnode11 = new SysOrgRoleNode();
        rnode11.setId("rnode11");
        rnode11.setName("一部部门领导");
        rnode11.setTreid("rtree1");
        rnode11.setOrnum(1);
        rnode11.setAvtag(true);
        rnode11.setMember(new SysOrg("c2"));
        rnode11.setParent(new SysOrgRoleNode("rnode1"));
        rnode11.setTier("xrnode1xrnode11x");
        roleNodeService.insert(rnode11);

        SysOrgRoleNode rnode111 = new SysOrgRoleNode();
        rnode111.setId("rnode111");
        rnode111.setName("一部人员");
        rnode111.setTreid("rtree1");
        rnode111.setOrnum(1);
        rnode111.setAvtag(true);
        rnode111.setMember(new SysOrg("dept1"));
        rnode111.setParent(new SysOrgRoleNode("rnode11"));
        rnode111.setTier("xrnode1xrnode11xrnode111x");
        roleNodeService.insert(rnode111);

        SysOrgRoleNode rnode2 = new SysOrgRoleNode();
        rnode2.setId("rnode2");
        rnode2.setName("二部分管领导");
        rnode2.setTreid("rtree1");
        rnode2.setOrnum(2);
        rnode2.setAvtag(true);
        rnode2.setMember(new SysOrg("post2"));
        rnode2.setTier("xrnode2x");
        roleNodeService.insert(rnode2);

        SysOrgRoleNode rnode21 = new SysOrgRoleNode();
        rnode21.setId("rnode21");
        rnode21.setName("二部部门领导");
        rnode21.setTreid("rtree1");
        rnode21.setOrnum(1);
        rnode21.setAvtag(true);
        rnode21.setMember(new SysOrg("z6"));
        rnode21.setParent(new SysOrgRoleNode("rnode2"));
        rnode21.setTier("xrnode2xrnode21x");
        roleNodeService.insert(rnode21);

        SysOrgRoleNode rnode211 = new SysOrgRoleNode();
        rnode211.setId("rnode211");
        rnode211.setName("二部人员");
        rnode211.setTreid("rtree1");
        rnode211.setOrnum(1);
        rnode211.setAvtag(true);
        rnode211.setMember(new SysOrg("dept2"));
        rnode211.setParent(new SysOrgRoleNode("rnode21"));
        rnode211.setTier("xrnode2xrnode21xrnode211x");
        roleNodeService.insert(rnode211);

    }


    private void addPostionDept(List<SysOrgDept> deptList,String id){
//        SysOrgDept qf=new SysOrgDept();
//        qf.setId(id+"qf");
//        qf.setName(StrUtil.toUpperfirst(id)+"_前锋");
//        qf.setLabel("1");
//        qf.setOrnum(1);
//        qf.setAvtag(true);
//        qf.setTier("x"+rootId+"x"+id.substring(0,1)+"x"+id+"x"+id+"qf"+"x");
//        qf.setType(2);
//        qf.setParent(new SysOrg(id));
//        deptList.add(qf);
//
//        SysOrgDept zc=new SysOrgDept();
//        zc.setId(id+"zc");
//        zc.setName(StrUtil.toUpperfirst(id)+"_中场");
//        zc.setLabel("1");
//        zc.setOrnum(2);
//        zc.setAvtag(true);
//        zc.setTier("x"+rootId+"x"+id.substring(0,1)+"x"+id+"x"+id+"zc"+"x");
//        zc.setType(2);
//        zc.setParent(new SysOrg(id));
//        deptList.add(zc);
//
//        SysOrgDept hw=new SysOrgDept();
//        hw.setId(id+"hw");
//        hw.setName(StrUtil.toUpperfirst(id)+"_后卫");
//        hw.setLabel("1");
//        hw.setOrnum(3);
//        hw.setAvtag(true);
//        hw.setTier("x"+rootId+"x"+id.substring(0,1)+"x"+id+"x"+id+"hw"+"x");
//        hw.setType(2);
//        hw.setParent(new SysOrg(id));
//        deptList.add(hw);
//
//        SysOrgDept mj=new SysOrgDept();
//        mj.setId(id+"mj");
//        mj.setName(StrUtil.toUpperfirst(id)+"_门将");
//        mj.setLabel("1");
//        mj.setOrnum(4);
//        mj.setAvtag(true);
//        mj.setTier("x"+rootId+"x"+id.substring(0,1)+"x"+id+"x"+id+"mj"+"x");
//        mj.setType(2);
//        mj.setParent(new SysOrg(id));
//        deptList.add(mj);

    }

    @Autowired
    private SysOrgRoleNodeService roleNodeService;

    @Autowired
    private SysOrgRoleTreeService roleTreeService;

    @Autowired
    private SysOrgGroupCateService groupCateService;

    @Autowired
    private SysOrgGroupService groupService;

    @Autowired
    private  SysOrgDeptService deptService;

    @Autowired
    private  SysOrgUserService userService;

    @Autowired
    private SysOrgPostService postService;

}
