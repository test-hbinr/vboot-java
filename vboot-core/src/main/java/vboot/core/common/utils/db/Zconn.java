package vboot.core.common.utils.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//数据库连接Pojo
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Zconn {
    private String name;

    private String type;

    private String url;

    private String username;

    private String passcode;

    public String getSid()
    {
        if(isMysql())
        {
            String lasturl=url.split("\\?")[0];
            return lasturl.substring(lasturl.lastIndexOf("/")+1);
        }
        else if (isOracle())
        {
            if(url.indexOf("/")>0){
                return url.substring(url.lastIndexOf("/")+1);
            }else{
                return url.substring(url.lastIndexOf(":")+1);
            }
        }
        else if (isSqlserver())
        {
            return url.substring(url.lastIndexOf("Name=")+5);
        }
        else if (isDb2())
        {
            return url.substring(url.lastIndexOf("/")+1);
        }
        else{
            return null;
        }
    }

    public String getDriver(){
        if(isOracle()){
            return "oracle.jdbc.driver.OracleDriver";
        }else if(isMysql()){
            return "com.mysql.jdbc.Driver";
        }else if(isMysql8()){
            return "com.mysql.cj.jdbc.Driver";
        }else if(isDb2()){
            return "com.ibm.db2.jcc.DB2Driver";
        }else{
            return "暂不支持";
        }
    }

    public boolean isOracle()
    {
        return DbType.ORACLE.equals(type);
    }
    public boolean isMysql()
    {
        return DbType.MYSQL.equals(type);
    }
    public boolean isMysql8()
    {
        return DbType.MYSQL8.equals(type);
    }

    public boolean isDb2()
    {
        return DbType.DB2.equals(type);
    }

    public boolean isSqlserver()
    {
        return DbType.SQL_SERVER.equals(type);
    }
}
