package vboot.core.common.utils.lang;

import cn.hutool.core.util.IdUtil;
import cn.hutool.crypto.SecureUtil;

import java.util.Random;

public class IdUtils {

    public static String getUID()
    {
        return IdUtil.getSnowflake().nextIdStr();
    }

    public static String getRSID(int length) {
        String val = "";
        Random random = new Random();
        //参数length，表示生成几位随机数
        for(int i = 0; i < length; i++) {
            String charOrNum = random.nextInt(2) % 2 == 0 ? "char" : "num";
            //输出字母还是数字
            if( "char".equalsIgnoreCase(charOrNum) ) {
                //输出是大写字母还是小写字母
//                int temp = random.nextInt(2) % 2 == 0 ? 65 : 97;
                val += (char)(random.nextInt(26) + 97);
            } else if( "num".equalsIgnoreCase(charOrNum) ) {
                val += String.valueOf(random.nextInt(10));
            }
        }
        return val;
    }

    public static String getRSID4() { return getRSID(4); }

    public static String getRSID8() {
        return getRSID(8);
    }

}
