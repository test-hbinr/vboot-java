package vboot.core.common.mvc.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.List;

@Data
public class Ztree {
    private String id;

    private String name;

    @JsonIgnore
    private String pid;

    private String type;

    private List<Ztree> children;

}