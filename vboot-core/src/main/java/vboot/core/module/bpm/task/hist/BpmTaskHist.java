package vboot.core.module.bpm.task.hist;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Getter
@Setter
@ApiModel("流程任务历史信息")
public class BpmTaskHist {

    @Id
    @Column(length = 32)
    @ApiModelProperty("主键ID")
    private String id;

    @Column(length = 32)
    @ApiModelProperty("任务类型")
    private String type;

    @Column(length = 32)
    @ApiModelProperty("流程实例id")
    private String proid;

    @Column(length = 32)
    @ApiModelProperty("节点id")
    private String nodid;

    @Column(updatable = false)
    @ApiModelProperty("开始时间")
    private Date sttim = new Date();

    @ApiModelProperty("结束时间")
    private Date entim;

    @Column(length = 8)
    @ApiModelProperty("消息类型")
    private String notty;

    @Column(length = 8)
    @ApiModelProperty("状态")
    private String state;

    @Column(length = 32)
    @ApiModelProperty("实处理人")
    private String haman;

    @Column(length = 32)
    @ApiModelProperty("授权处理人")
    private String auman;

    @Column(length = 32)
    @ApiModelProperty("应处理人")
    private String exman;

}
