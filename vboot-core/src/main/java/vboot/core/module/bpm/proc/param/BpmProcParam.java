package vboot.core.module.bpm.proc.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@ApiModel("流程参数信息")
public class BpmProcParam {

    @Id
    @Column(length = 32)
    @ApiModelProperty("主键ID")
    private String id;

    @Column(length = 64)
    @ApiModelProperty("参数Key")
    private String pakey;

    @Column(length = 512)
    @ApiModelProperty("参数值")
    private String paval;

    @Column(length = 32)
    @ApiModelProperty("OFF类型")
    private String offty;//这个是什么？

    @Column(length = 32)
    @ApiModelProperty("OFFID")
    private String offid;//这个是什么？

    @Column(length = 32)
    @ApiModelProperty("流程ID")
    private String proid;


}
