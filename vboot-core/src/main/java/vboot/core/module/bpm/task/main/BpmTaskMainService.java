package vboot.core.module.bpm.task.main;

import cn.hutool.core.util.StrUtil;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.module.bpm.proc.main.Zbpm;
import vboot.core.module.bpm.proc.main.Znode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vboot.core.common.utils.lang.IdUtils;
import vboot.core.common.mvc.pojo.Ztree;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Transactional(rollbackFor = Exception.class)
@Service
public class BpmTaskMainService {

    public List<BpmTaskMain> findAllByProidNotActive(String proid){
        return repo.findAllByProidAndActagOrderByOrnum(proid,false);
    }

    public List<BpmTaskMain> findAllByProid(String proid){
        return repo.findAllByProidOrderByOrnum(proid);
    }

    public BpmTaskMain createTask(Zbpm zbpm, Znode znode) {
        BpmTaskMain task = new BpmTaskMain();
        task.setId(IdUtils.getUID());
        task.setProid(zbpm.getProid());
        task.setState("20");
        task.setExman(znode.getExmen());
        task.setOrnum(0);
        task.setActag(true);
        task.setNodid(znode.getNodid());
        task.setType(znode.getFacty());
        return repo.save(task);
    }

    public List<BpmTaskMain> createTaskList(Zbpm zbpm, Znode znode) {
        List<BpmTaskMain> list = new ArrayList<>();
        if (StrUtil.isNotBlank(znode.getExmen()) && !znode.getExmen().contains(";")) {
            BpmTaskMain task = new BpmTaskMain();
            task.setId(IdUtils.getUID());
            task.setProid(zbpm.getProid());
            task.setState("20");
            task.setExman(znode.getExmen());
            task.setOrnum(0);
            task.setActag(true);
            task.setNodid(znode.getNodid());
            task.setType(znode.getFacty());
            list.add(repo.save(task));
        } else if ("1".equals(znode.getFlway())) {
            String[] ids = znode.getExmen().split(";");
            for (int i = 0; i < ids.length; i++) {
                BpmTaskMain task = new BpmTaskMain();
                task.setId(IdUtils.getUID());
                task.setProid(zbpm.getProid());
                task.setState("20");
                task.setExman(ids[i]);
                task.setOrnum(i);
                if (i == 0) {
                    task.setActag(true);
                } else {
                    task.setActag(false);
                }
                task.setNodid(znode.getNodid());
                task.setType(znode.getFacty());
                list.add(repo.save(task));
            }
        } else if ("2".equals(znode.getFlway())||"3".equals(znode.getFlway())) {
            String[] ids = znode.getExmen().split(";");
            for (int i = 0; i < ids.length; i++) {
                BpmTaskMain task = new BpmTaskMain();
                task.setId(IdUtils.getUID());
                task.setProid(zbpm.getProid());
                task.setState("20");
                task.setExman(ids[i]);
                task.setOrnum(i);
                task.setActag(true);
                task.setNodid(znode.getNodid());
                task.setType(znode.getFacty());
                list.add(repo.save(task));
            }
        }
        return list;
    }

    @Transactional(readOnly = true)
    public BpmTaskMain findOne(String id) {
        return repo.findById(id).get();
    }

    public void delete(String id) {
        repo.deleteById(id);
    }

    public void deleteAllByProid(String proid) {
        repo.deleteAllByProid(proid);
    }


    public void findCurrentExmen(List<Map<String, Object>> itemList) {
        String ids = "(";
        for (Map<String, Object> map : itemList) {
            if (!"30".equals(map.get("state"))) {
                ids += "'" + map.get("id") + "',";
            }
        }
        if (!"(".equals(ids)) {
            ids = ids.substring(0, ids.length() - 1) + ")";
            Sqler sqler = new Sqler("n.id as tasid,t.id as nodid,o.name exnam,n.exman,t.proid,t.facno,t.facna", "bpm_node_main");
            sqler.addInnerJoin("", "bpm_task_main n", "n.nodid=t.id");
            sqler.addInnerJoin("", "sys_org o", "o.id=n.exman");
            sqler.addWhere("t.proid in " + ids + " and n.actag=1");
            sqler.addOrder("t.proid");
            sqler.addOrder("n.ornum");

            List<Map<String, Object>> tasks = jdbcDao.findMapList(sqler);

            List<Ztree> list = new ArrayList<>();
            String proid = "";
            for (Map<String, Object> task : tasks) {
                if (!proid.equals(task.get("proid"))) {
                    Ztree ztree = new Ztree();
                    ztree.setId(task.get("proid") + "");
                    ztree.setName(task.get("facno") + "." + task.get("facna"));
                    ztree.setPid((String) task.get("exnam"));
                    list.add(ztree);
                } else {
                    list.get(list.size() - 1).setPid(list.get(list.size() - 1).getPid() + ";" + task.get("exnam"));
                }
                proid = (String) task.get("proid");
            }

            for (Map<String, Object> map : itemList) {
                for (Ztree ztree : list) {
                    if (ztree.getId().equals(map.get("id"))) {
                        map.put("facno", ztree.getName());
                        map.put("exmen", ztree.getPid());
                        break;
                    }
                }
            }
        }
    }

    @Autowired
    private JdbcDao jdbcDao;

    @Autowired
    private BpmTaskMainRepo repo;
}
