package vboot.core.module.bpm.proc.main;

import cn.hutool.core.util.StrUtil;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.mvc.service.BaseMainService;
import vboot.core.common.utils.db.DbType;
import vboot.core.common.utils.web.XuserUtil;
import vboot.core.module.bpm.audit.main.BpmAuditMainService;
import vboot.core.module.bpm.node.hist.BpmNodeHist;
import vboot.core.module.bpm.node.hist.BpmNodeHistService;
import vboot.core.module.bpm.node.main.BpmNodeMain;
import vboot.core.module.bpm.node.main.BpmNodeMainService;
import vboot.core.module.bpm.proc.param.BpmProcParam;
import vboot.core.module.bpm.proc.param.BpmProcParamService;
import vboot.core.module.bpm.task.hist.BpmTaskHist;
import vboot.core.module.bpm.task.hist.BpmTaskHistService;
import vboot.core.module.bpm.task.main.BpmTaskMain;
import vboot.core.module.bpm.task.main.BpmTaskMainService;
import vboot.core.module.sys.org.role.SysOrgRoleTreeService;
import vboot.core.module.sys.org.root.SysOrg;
import vboot.core.module.sys.todo.main.SysTodoMainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Service;
import vboot.core.common.utils.lang.IdUtils;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class BpmProcMainService extends BaseMainService<BpmProcMain> {

    //启动流程
    public Znode start(Zbpm zbpm) throws Exception {
        //1 保存流程实例
        BpmProcMain bpmProcMain = new BpmProcMain(zbpm);
        super.insert(bpmProcMain);

        //2 历史节点表保存开始节点
        nodeHistService.saveStartNode(zbpm);

        //3 流程流转（收集流转过的节点，计算出下一个待审批节点）
        Znode draftNode = new Znode("N1");
        draftNode.setFacna("起草节点");
        draftNode.setFacty("draft");
        List<Znode> list = new ArrayList<>();

        String xmlSql = "select t.chxml from bpm_proc_temp t where t.id=?";
        String chxml = jdbcDao.findOneString(xmlSql,zbpm.getTemid());
        zbpm.setChxml(chxml);
        Znode nextNode = hand.procFlow(zbpm, list, draftNode);//流转核心逻辑
        if(nextNode==null){
            throw new Exception("未找到下一个可流的流程节点");
        }
        exmenTran(null,nextNode);//审批人转换

        //4.1 历史节点表保存起草节点
        draftNode.setNodid(IdUtils.getUID());
        nodeHistService.saveDraftNode(zbpm, draftNode);
        //4.2 评审表保存起草节点的评审信息
        auditMainService.saveDraftAudit(zbpm, draftNode);
        //4.3 历史节点表保存其他已流节点（条件分支等非审批节点）
        nodeHistService.saveNodeList(zbpm, list);
        //4.4 当前节点表保存下一个待审批节点
        BpmNodeMain nodeMain = nodeMainService.saveNode(zbpm, nextNode);
        nextNode.setNodid(nodeMain.getId());
        //4.5 历史节点表保存下一个待审批节点
        nodeHistService.saveNode(nodeMain);

        //5.1 当前任务表创建待审节点的任务
        List<BpmTaskMain> mainTaskList = taskMainService.createTaskList(zbpm, nextNode);
        //5.2 历史任务表创建待审节点的任务
        taskHistService.createTaskList(mainTaskList);

        //6 发起待办
        todoService.sendTodos(zbpm, mainTaskList);
        return nextNode;
    }

    //通过流程
    public Znode handlerPass(Zbpm zbpm) throws Exception {
        zbpm.setHaman(XuserUtil.getUserId());

        String sql = "select m.id as \"proid\",m.name \"prona\" from bpm_proc_main m where m.id=?";
        Map<String, Object> map = jdbcDao.findMap(sql, zbpm.getProid());
        zbpm.setProna((String)map.get("prona"));

        zbpm.setOpkey("pass");
        zbpm.setOpinf("通过");

        //1 评审表保存当前节点的评审信息
        auditMainService.saveAudit(zbpm);
        if(StrUtil.isNotBlank(zbpm.getBacid())){
            paramService.delete(zbpm.getBacid());
        }

        //2.1 将历史任务变成已办
        BpmTaskHist histTask = taskHistService.findOne(zbpm.getTasid());
        histTask.setHaman(zbpm.getHaman());
        histTask.setEntim(new Date());
        histTask.setState("30");
        //2.2 删除当前任务表记录
        taskMainService.delete(zbpm.getTasid());


        //3 流程流转
        Znode currNode = new Znode(zbpm.getFacno());
        currNode.setNodid(zbpm.getNodid());
        currNode.setFacno(zbpm.getFacno());
        currNode.setFacty("review");
        if(StrUtil.isNotBlank(zbpm.getBacid())){
            currNode.setTarno(zbpm.getTarno());
            currNode.setTarna(zbpm.getTarna());
        }
        BpmNodeMain dbNode = nodeMainService.findOne(currNode.getNodid());
        if("1".equals(dbNode.getFlway())){
            List<BpmTaskMain> bpmTaskMainList = taskMainService.findAllByProidNotActive(zbpm.getProid());
            if(bpmTaskMainList.size()>0){
                bpmTaskMainList.get(0).setActag(true);
                taskHistService.createTask(bpmTaskMainList.get(0));
                //7.1 删除之前的待办
                todoService.doneTodos(zbpm);
                //7.2 发起新待办
                todoService.sendTodos(zbpm, bpmTaskMainList);
                return currNode;
            }
        }else if("2".equals(dbNode.getFlway())){
            List<BpmTaskMain> bpmTaskMainList = taskMainService.findAllByProid(zbpm.getProid());
            for (BpmTaskMain bpmTaskMain : bpmTaskMainList) {
                taskMainService.delete(bpmTaskMain.getId());
                taskHistService.delete(bpmTaskMain.getId());
            }
        }else if("3".equals(dbNode.getFlway())){
            List<BpmTaskMain> bpmTaskMainList = taskMainService.findAllByProid(zbpm.getProid());
            if(bpmTaskMainList.size()>0){
                todoService.doneTodo(zbpm);
                return currNode;
            }
        }


        List<Znode> list = new ArrayList<>();
        String xmlSql = "select t.chxml from bpm_proc_temp t " +
                "inner join bpm_proc_main m on m.temid=t.id  where m.id=?";
        String chxml =   jdbcDao.findOneString(xmlSql, zbpm.getProid());;
        zbpm.setChxml(chxml);

        Znode nextNode = hand.procFlow(zbpm, list, currNode);//流转核心逻辑
        if(nextNode==null){
            throw new Exception("未找到下一个可流的流程节点");
        }
        exmenTran(zbpm.getProid(),nextNode);//审批人转换

        //4.1 将历史节点变成已办
        BpmNodeHist histNode = nodeHistService.findOne(zbpm.getNodid());
        histNode.setEntim(new Date());
        histNode.setState("30");
        histNode.setTarno(currNode.getTarno());
        histNode.setTarna(currNode.getTarna());
        currNode.setFacna(histNode.getFacna());
        //4.2 历史节点表保存已流节点
        nodeHistService.saveNodeList(zbpm, list);
        //4.3 删除当前节点表记录
        nodeMainService.delete(zbpm.getNodid());

        if (!"end".equals(nextNode.getFacty())) {
            //5.1 当前节点表保存下一个待审批节点
            BpmNodeMain nodeMain = nodeMainService.saveNode(zbpm, nextNode);
            nextNode.setNodid(nodeMain.getId());
            //5.2 历史节点表保存下一个待审批节点
            nodeHistService.saveNode(nodeMain);

            //6.1 当前任务表创建待审节点的任务
            List<BpmTaskMain> mainTaskList = taskMainService.createTaskList(zbpm, nextNode);
            //6.2 历史任务表创建待审节点的任务
            taskHistService.createTaskList(mainTaskList);

            //7.1 删除之前的待办
            todoService.doneTodos(zbpm);
            //7.2 发起新待办
            todoService.sendTodos(zbpm, mainTaskList);
        } else {
            //5 历史节点表保存结束节点
            String endNodeId=nodeHistService.saveEndNode(zbpm);

            //6 评审表保存结束节点的评审信息
            auditMainService.saveEndAudit(zbpm,endNodeId);

            //7 删除之前的待办
            todoService.doneTodos(zbpm);

            //8 将流程更新成完结
            String procUpdateSql = "update bpm_proc_main set state='30' where id=?";
            jdbcDao.update(procUpdateSql,zbpm.getProid());
        }

        return nextNode;
    }

    //驳回流程
    public Znode handlerRefuse(Zbpm zbpm) {
        zbpm.setHaman(XuserUtil.getUserId());

        String sql = "select m.id \"proid\",m.name \"prona\" from bpm_proc_main m where m.id=?";
        Map<String, Object> map = jdbcDao.findMap(sql, zbpm.getProid());
        zbpm.setProna((String) map.get("prona"));

        //驳回: "起草节点"（返回本人）
        if(zbpm.getRetag()){
            zbpm.setOpinf("驳回: "+zbpm.getTarno()+"."+zbpm.getTarna()+"（返回本人）");
        }else{
            zbpm.setOpinf("驳回: "+zbpm.getTarno()+"."+zbpm.getTarna());
        }

        //1 评审表保存当前节点的评审信息
        auditMainService.saveAudit(zbpm);

        //2.1 将历史任务变成已办
        BpmTaskHist histTask = taskHistService.findOne(zbpm.getTasid());
        histTask.setHaman(zbpm.getHaman());
        histTask.setEntim(new Date());
        histTask.setState("30");
        //2.2 删除当前任务表记录
        taskMainService.deleteAllByProid(zbpm.getProid());
        //2.3 删除历史任务未结束记录
        taskHistService.deleteAllByProidNotEnd(zbpm.getProid());

        //3 创建驳回节点
//        Znode refuseNode = hand.getNodeInfo(zbpm,zbpm.getTarno());
//        Znode refuseNode = new Znode();
//        refuseNode.setFacno(zbpm.getTarno());
//        refuseNode.setFacna(zbpm.getTarna());
//        refuseNode.setExmen(zbpm.getExman());
//        refuseNode.setFacty("review");
        String xmlSql = "select m.crmid \"crmid\",t.chxml \"chxml\" from bpm_proc_temp t inner join bpm_proc_main m on m.temid=t.id where m.id=?";
        Map<String,Object> xmlMap = jdbcDao.findMap(xmlSql,zbpm.getProid());
        Znode refuseNode=hand.getNodeInfo(xmlMap.get("chxml")+"",zbpm.getTarno());
        if("N1".equals(refuseNode.getFacno())){
            refuseNode.setExmen(xmlMap.get("crmid")+"");
        }
        exmenTran(zbpm.getProid(),refuseNode);
        //4.1 将历史节点变成已办
        BpmNodeHist histNode = nodeHistService.findOne(zbpm.getNodid());
        histNode.setTarno(zbpm.getTarno());
        histNode.setTarna(zbpm.getTarna());
        histNode.setEntim(new Date());
        histNode.setState("30");
        //4.2 删除当前节点表记录
        nodeMainService.delete(zbpm.getNodid());

        //5.1 当前节点表保存下一个待审批节点
        BpmNodeMain nodeMain = nodeMainService.saveNode(zbpm, refuseNode);
        refuseNode.setNodid(nodeMain.getId());
        //5.2 历史节点表保存下一个待审批节点
        nodeHistService.saveNode(nodeMain);
        //5.3 如果驳回时勾选了 驳回的节点通过后直接返回本节点
        if(zbpm.getRetag()==true){
            BpmProcParam param = new BpmProcParam();
            param.setId(IdUtils.getUID());
            param.setProid(zbpm.getProid());
            param.setOffty("proc");
            param.setOffid(zbpm.getProid());
            param.setPakey(zbpm.getTarno()+"#refuse");
            param.setPaval(zbpm.getFacno());
            paramService.save(param);
        }

        //6.1 当前任务表创建待审节点的任务
        List<BpmTaskMain> mainTaskList = taskMainService.createTaskList(zbpm, refuseNode);
        //6.2 历史任务表创建待审节点的任务
        taskHistService.createTaskList(mainTaskList);

        //7.1 删除之前的待办
        todoService.doneTodos(zbpm);
        //7.2 发起新待办
        todoService.sendTodos(zbpm, mainTaskList);
        return refuseNode;
    }

    //转办流程
    public void handlerTurn(Zbpm zbpm) {
        zbpm.setHaman(XuserUtil.getUserId());

        String sql = "select m.id \"proid\",m.name \"prona\" from bpm_proc_main m where m.id=?";
        Map<String, Object> map = jdbcDao.findMap(sql, zbpm.getProid());
        zbpm.setProna((String)map.get("prona"));

        String turnManSql = "select id,name,type from sys_org where id=?";
        SysOrg sysOrg = jdbcDao.getTp().queryForObject(turnManSql,
                new Object[]{zbpm.getTumid()}, new BeanPropertyRowMapper<>(SysOrg.class));
        if(zbpm.getTutag()){
            zbpm.setOpinf("转办: "+sysOrg.getName()+"（完整转办）");
        }else{
            zbpm.setOpinf("转办: "+sysOrg.getName());
        }
        //1 评审表保存当前节点的评审信息
        auditMainService.saveAudit(zbpm);

        BpmTaskMain task = taskMainService.findOne(zbpm.getTasid());
        task.setExman(zbpm.getTumid());

        todoService.doneTodo(zbpm);
        todoService.sendTodo(zbpm,zbpm.getTumid());

    }

    //沟通流程
    public void handlerCommunicate(Zbpm zbpm) {
        zbpm.setHaman(XuserUtil.getUserId());

        String sql = "select m.id \"proid\",m.name \"prona\" from bpm_proc_main m where m.id=?";
        Map<String, Object> map = jdbcDao.findMap(sql, zbpm.getProid());
        zbpm.setProna((String)map.get("prona"));

        String names=getOrgNames("'" + zbpm.getCoids().replaceAll(";", "','") + "'");
//        Sqler sqler = new Sqler("sys_org");
//        sqler.addWhere("id in " + "(" + ids + ")");
//        if (DbType.MYSQL.equals(jdbcDao.getDbType())) {
//            sqler.addOrder("field(id," + ids + ")");
//        }else if(DbType.ORACLE.equals(jdbcDao.getDbType())){
//            sqler.addOrder("INSTR('"+ids.replaceAll("'","")+"',id)");
//        }else if(DbType.SQL_SERVER.equals(jdbcDao.getDbType())){
//            sqler.addOrder("CHARINDEX(id,'"+ids.replaceAll("'","")+"')");
//        }
//
//        sqler.addSelect("t.type");
//        List<SysOrg> list = jdbcDao.getTp().query(sqler.getSql(), sqler.getParams(),
//                new BeanPropertyRowMapper<>(SysOrg.class));
//        String names="";
//        for (SysOrg sysOrg : list) {
//            names += sysOrg.getName() + ";";
//        }
//        names = names.substring(0, names.length() - 1);
        if(zbpm.getCotag()){
            zbpm.setOpinf("沟通："+names+"（隐藏意见）");
        }else{
            zbpm.setOpinf("沟通："+names);
        }
        //1 评审表保存当前节点的评审信息
        auditMainService.saveAudit(zbpm);

        Znode currNode = new Znode(zbpm.getFacno());
        currNode.setNodid(zbpm.getNodid());
        currNode.setFacno(zbpm.getFacno());
        currNode.setFacty("communicate");
        currNode.setExmen(zbpm.getCoids());
        currNode.setFlway("3");

        //6.1 将当前任务置为沟通状态
        jdbcDao.update("update bpm_task_main set type='to_communicate' where id=?",zbpm.getTasid());
        //6.2 当前任务表创建待审节点的任务
        List<BpmTaskMain> mainTaskList = taskMainService.createTaskList(zbpm, currNode);
        //6.3 历史任务表创建待审节点的任务
        taskHistService.createTaskList(mainTaskList);

        //7.2 发起新待办
        todoService.sendTodos(zbpm, mainTaskList);

    }

    //沟通回复
    public void handlerBacommunicate(Zbpm zbpm) {
        zbpm.setHaman(XuserUtil.getUserId());

        zbpm.setOpinf("沟通回复");
        //1 评审表保存当前节点的评审信息
        auditMainService.saveAudit(zbpm);

        //2.1 将历史任务变成已办
        BpmTaskHist histTask = taskHistService.findOne(zbpm.getTasid());
        histTask.setHaman(zbpm.getHaman());
        histTask.setEntim(new Date());
        histTask.setState("30");
        //2.2 删除当前任务表记录
        taskMainService.delete(zbpm.getTasid());

        todoService.doneTodo(zbpm);

//        Znode currNode = new Znode(zbpm.getFacno());
//        currNode.setNodid(zbpm.getNodid());
//        currNode.setFacno(zbpm.getFacno());
//        currNode.setFacty("communicate");
//        currNode.setExmen(zbpm.getCoids());
//        currNode.setFlway("3");
//
//        //6.1 当前任务表创建待审节点的任务
//        List<BpmTaskMain> mainTaskList = taskMainService.createTaskList(zbpm, currNode);
//        //6.2 历史任务表创建待审节点的任务
//        taskHistService.createTaskList(mainTaskList);
//        //7.2 发起新待办
//        todoService.sendTodos(zbpm, mainTaskList);

    }

    //取消沟通
    public void handlerCacommunicate(Zbpm zbpm) {

        //2.1 将历史任务变成已办
        String[] taskIds= zbpm.getCcids().split(",");
        String userIds="";
        for (String taskId : taskIds) {
            BpmTaskHist histTask = taskHistService.findOne(taskId);
            histTask.setHaman(zbpm.getHaman());
            histTask.setEntim(new Date());
            histTask.setState("30");
            userIds+=histTask.getExman()+",";
            //2.2 删除当前任务表记录
            taskMainService.delete(taskId);
        }
        userIds=userIds.substring(0,userIds.length()-1);
        zbpm.setHaman(XuserUtil.getUserId());
        String names=getOrgNames("'" + userIds.replaceAll(",", "','") + "'");
        zbpm.setOpinf("取消沟通："+names);
        //1 评审表保存当前节点的评审信息
        auditMainService.saveAudit(zbpm);

        todoService.doneTodosByTaskIds(taskIds);
        repo.flush();
        String sql = "select count(1) from bpm_task_main t where t.proid=? and t.type='communicate'";
        Integer count = jdbcDao.getTp().queryForObject(sql, new Object[]{zbpm.getProid()}, Integer.class);
        //如果取消了所有沟通人员，则更新状态
        if(count!=null&&count==0){
            jdbcDao.update("update bpm_task_main set type='review' where id=?",zbpm.getTasid());
        }
//        Znode currNode = new Znode(zbpm.getFacno());
//        currNode.setNodid(zbpm.getNodid());
//        currNode.setFacno(zbpm.getFacno());
//        currNode.setFacty("communicate");
//        currNode.setExmen(zbpm.getCoids());
//        currNode.setFlway("3");
//
//        //6.1 当前任务表创建待审节点的任务
//        List<BpmTaskMain> mainTaskList = taskMainService.createTaskList(zbpm, currNode);
//        //6.2 历史任务表创建待审节点的任务
//        taskHistService.createTaskList(mainTaskList);
//        //7.2 发起新待办
//        todoService.sendTodos(zbpm, mainTaskList);

    }

    //废弃流程
    public void handlerAbandon(Zbpm zbpm) {
        zbpm.setHaman(XuserUtil.getUserId());
//        String sql = "select m.id \"proid\",m.name \"prona\" from bpm_proc_main m where m.id=?";
//        Map<String, Object> map = jdbcDao.findMap(sql, zbpm.getProid());
//        zbpm.setProna((String) map.get("prona"));

        zbpm.setOpinf("废弃");
        //1 评审表保存当前节点的评审信息
        auditMainService.saveAudit(zbpm);
        //2.1 将历史任务变成已办
        BpmTaskHist histTask = taskHistService.findOne(zbpm.getTasid());
        histTask.setHaman(zbpm.getHaman());
        histTask.setEntim(new Date());
        histTask.setState("30");
        //2.2 删除当前任务表记录
        taskMainService.deleteAllByProid(zbpm.getProid());
        //2.3 删除历史任务未结束记录
        taskHistService.deleteAllByProidNotEnd(zbpm.getProid());

        //4.1 将历史节点变成已办
        BpmNodeHist histNode = nodeHistService.findOne(zbpm.getNodid());
        histNode.setTarno(zbpm.getTarno());
        histNode.setTarna(zbpm.getTarna());
        histNode.setEntim(new Date());
        histNode.setState("30");
        //4.2 删除当前节点表记录
        nodeMainService.delete(zbpm.getNodid());

        //7.1 删除之前的待办
        todoService.doneTodos(zbpm);
    }
    private void exmenTran(String proid,Znode znode) {
        String tamen = "";
        if (StrUtil.isNotBlank(znode.getExmen()) && !znode.getExmen().contains(";")) {
            String tamenSql = "select t.id, t.name,t.type from sys_org t where t.id=?";
            SysOrg sysOrg = jdbcDao.getTp().queryForObject(tamenSql,
                    new Object[]{znode.getExmen()}, new BeanPropertyRowMapper<>(SysOrg.class));
            if (sysOrg.getType()==32) {
                String crmid=XuserUtil.getUserId();
                if(proid!=null){
                    String sql="select crmid from bpm_proc_main where id=?";
                    crmid= jdbcDao.findOneString(sql,proid);
                }
                SysOrg org = sysOrgRoleTreeService.calc(crmid, sysOrg.getId());
                tamen = org.getId();
            } else {
                tamen = sysOrg.getId();
            }
        } else if (StrUtil.isNotBlank(znode.getExmen()) && znode.getExmen().contains(";")) {
            Sqler sqler = new Sqler("sys_org");
            String ids = znode.getExmen();
            ids = "'" + ids.replaceAll(";", "','") + "'";
            sqler.addWhere("id in " + "(" + ids + ")");
            if (DbType.MYSQL.equals(jdbcDao.getDbType())) {
                sqler.addOrder("field(id," + ids + ")");
            }else if(DbType.ORACLE.equals(jdbcDao.getDbType())){
                sqler.addOrder("INSTR('"+ids.replaceAll("'","")+"',id)");
            }else if(DbType.SQL_SERVER.equals(jdbcDao.getDbType())){
                sqler.addOrder("CHARINDEX(id,'"+ids.replaceAll("'","")+"')");
            }
            sqler.addSelect("t.type");
            System.out.println(sqler.getSql());
            List<SysOrg> list = jdbcDao.getTp().query(sqler.getSql(), sqler.getParams(),
                    new BeanPropertyRowMapper<>(SysOrg.class));
//            List<ZidName> idNameList = jdbcDao.findIdNameList(sqler);
            for (SysOrg sysOrg : list) {
                if (sysOrg.getType() == 32) {
                    String crmid=XuserUtil.getUserId();
                    if(proid!=null){
                        String sql="select crmid from bpm_proc_main where id=?";
                        crmid= jdbcDao.findOneString(sql,proid);
                    }
                    SysOrg org = sysOrgRoleTreeService.calc(crmid, sysOrg.getId());
                    tamen += org.getId() + ";";
                } else {
                    tamen += sysOrg.getId() + ";";
                }
            }
            tamen = tamen.substring(0, tamen.length() - 1);
        }
        znode.setExmen(tamen);
    }

    public Zbpm getZbpm(String proid) {
        String sql = "select t.id as tasid,t.nodid,t.exman,t.proid,n.facno,n.facna from bpm_task_main t" +
                " inner join bpm_node_main n on n.id=t.nodid where t.proid=? and t.actag=1";
        List<Zbpm> list = jdbcDao.getTp().query(sql, new Object[]{proid},
                new BeanPropertyRowMapper<>(Zbpm.class));
        String userid = XuserUtil.getUserId();
        for (Zbpm zbpm : list) {
            if (userid.equals(zbpm.getExman())) {
                return zbpm;
            }
        }
        return null;
    }

    private String getOrgNames(String ids){
        Sqler sqler = new Sqler("sys_org");
        sqler.addWhere("id in " + "(" + ids + ")");
        if (DbType.MYSQL.equals(jdbcDao.getDbType())) {
            sqler.addOrder("field(id," + ids + ")");
        }else if(DbType.ORACLE.equals(jdbcDao.getDbType())){
            sqler.addOrder("INSTR('"+ids.replaceAll("'","")+"',id)");
        }else if(DbType.SQL_SERVER.equals(jdbcDao.getDbType())){
            sqler.addOrder("CHARINDEX(id,'"+ids.replaceAll("'","")+"')");
        }

        sqler.addSelect("t.type");
        List<SysOrg> list = jdbcDao.getTp().query(sqler.getSql(), sqler.getParams(),
                new BeanPropertyRowMapper<>(SysOrg.class));
        String names="";
        for (SysOrg sysOrg : list) {
            names += sysOrg.getName() + ";";
        }
        names = names.substring(0, names.length() - 1);
        return names;
    }

    //----------bean注入------------
    @Autowired
    private SysOrgRoleTreeService sysOrgRoleTreeService;

    @Autowired
    private SysTodoMainService todoService;

    @Autowired
    private BpmTaskHistService taskHistService;

    @Autowired
    private BpmTaskMainService taskMainService;

    @Autowired
    private BpmNodeHistService nodeHistService;

    @Autowired
    private BpmNodeMainService nodeMainService;

    @Autowired
    private BpmAuditMainService auditMainService;

    @Autowired
    private BpmProcParamService paramService;

    @Autowired
    private BpmProcMainHand hand;

    @Autowired
    private BpmProcMainRepo repo;

    @PostConstruct
    public void initDao() {
        super.setRepo(repo);
    }

}
