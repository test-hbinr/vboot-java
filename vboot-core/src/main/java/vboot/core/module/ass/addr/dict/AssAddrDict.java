package vboot.core.module.ass.addr.dict;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import vboot.core.common.mvc.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@Getter
@Setter
@ApiModel("区信息")
public class AssAddrDict extends BaseEntity {

    @Column(length = 64)
    @ApiModelProperty("坐标")
    private String cecoo;

    @Column(length = 32)
    @ApiModelProperty("城市")
    private String city;
}

