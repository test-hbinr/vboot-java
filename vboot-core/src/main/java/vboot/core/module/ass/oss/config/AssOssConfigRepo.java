package vboot.core.module.ass.oss.config;


import org.springframework.data.jpa.repository.JpaRepository;

public interface AssOssConfigRepo extends JpaRepository<AssOssConfig,String> {

}
