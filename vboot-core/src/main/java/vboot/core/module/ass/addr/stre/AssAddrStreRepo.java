package vboot.core.module.ass.addr.stre;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AssAddrStreRepo extends JpaRepository<AssAddrStre, String> {

}