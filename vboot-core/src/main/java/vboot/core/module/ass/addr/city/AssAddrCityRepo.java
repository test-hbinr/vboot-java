package vboot.core.module.ass.addr.city;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AssAddrCityRepo extends JpaRepository<AssAddrCity, String> {

}