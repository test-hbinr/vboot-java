package vboot.core.module.gen.img;

import com.google.zxing.WriterException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import vboot.core.common.utils.file.XqcodeUtil;

import java.io.IOException;
import java.io.InputStream;

@RestController
@RequestMapping("gen/img")
@Api(tags = {"图片服务"})
public class GenImgApi {

    @RequestMapping(value="qr",produces = MediaType.IMAGE_JPEG_VALUE)
    @ResponseBody
    @ApiOperation("生产图片二维码")
    public byte[] getImage(String label,Integer size) throws IOException, WriterException {
        if(size==null||size==0)
        {
            size=900;
        }
        InputStream inputStream= XqcodeUtil.createQrCode(label, size, "JPEG");
        byte[] bytes = new byte[inputStream.available()];
        inputStream.read(bytes, 0, inputStream.available());
        return bytes;
    }

}
