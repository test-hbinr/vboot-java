package vboot.core.module.gen.num;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import vboot.core.common.mvc.api.R;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vboot.core.common.utils.lang.IdUtils;

@RestController
@RequestMapping("gen/num")
@Api(tags = {"编号服务"})
public class GenNumApi {

    @GetMapping("uuid")
    @ApiOperation("获取唯一的ID")
    public R uuid() {
        return R.ok(IdUtils.getUID());
    }

}
