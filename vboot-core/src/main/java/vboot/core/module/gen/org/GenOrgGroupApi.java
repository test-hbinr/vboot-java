package vboot.core.module.gen.org;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.pojo.ZidName;
import vboot.core.common.mvc.pojo.Ztree;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("gen/org/group")
public class GenOrgGroupApi {

    @GetMapping("tree")
    public R getTree(String name) {
        String sql="select id,pid,name,'cate' type from sys_org_group_cate " +
                "union all select id,catid as pid,name,'group' type from sys_org_group";
        List<Ztree> list = jdbcDao.findTreeList(sql);
        return R.ok(list);
    }

    @GetMapping("list")
    public R getList(String pid,String type,String name) {
        if("cate".equals(type)){
            String sql="select id,name from sys_org_group where catid=? order by ornum";
            return R.ok(jdbcDao.findIdNameList(sql, pid));
        }else if("group".equals(type)){
            String sql="select t.id,t.name from sys_org t inner join sys_org_group_org o on o.oid=t.id where o.gid=?";
            return R.ok(jdbcDao.findIdNameList(sql, pid));
        }else{
            return R.ok(new ArrayList<ZidName>());
        }
    }


    @Autowired
    private JdbcDao jdbcDao;


}
