package vboot.core.module.sys.org.group;

import cn.hutool.core.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Service;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.mvc.pojo.ZidOrnum;
import vboot.core.common.mvc.pojo.TreeMovePo;
import vboot.core.common.mvc.service.BaseCateService;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
public class SysOrgGroupCateService extends BaseCateService<SysOrgGroupCate> {

    //获取分类treeTable数据
    public List<SysOrgGroupCate> findTree(Sqler sqler) {
        List<SysOrgGroupCate> list = jdbcDao.getTp().query(sqler.getSql(), sqler.getParams(),
                new BeanPropertyRowMapper<>(SysOrgGroupCate.class));
        return buildByRecursive(list);
    }

    public Integer getCount(String pid){
        if(StrUtil.isNotBlank(pid)){
            String countSql="select count(1) from sys_org_group_cate where pid=?";
            Integer count = jdbcDao.getTp().queryForObject(countSql, new Object[]{pid}, Integer.class);
            if(count==null){
                count=0;
            }
            return count;
        }else{
            String countSql="select count(1) from sys_org_group_cate where pid is null";
            Integer count = jdbcDao.getTp().queryForObject(countSql, Integer.class);
            if(count==null){
                count=0;
            }
            return count;
        }
    }

    public void move(TreeMovePo po) throws Exception {
        SysOrgGroupCate dragDept= repo.findById(po.getDraid()).get();
        if(dragDept.getParent()!=null){
            dragDept.setPid(dragDept.getParent().getId());
        }
        List<ZidOrnum> list2;
        if(StrUtil.isNotBlank(dragDept.getPid())){
            String sql = "select id,ornum from sys_org_group_cate where ornum>? and pid=?";
            list2 = jdbcDao.getTp().query(sql,new Object[]{dragDept.getOrnum(),dragDept.getPid()},
                    new BeanPropertyRowMapper<>(ZidOrnum.class));
        }else{
            String sql = "select id,ornum from sys_org_group_cate where ornum>? and pid is null";
            list2 = jdbcDao.getTp().query(sql,new Object[]{dragDept.getOrnum()},
                    new BeanPropertyRowMapper<>(ZidOrnum.class));
        }

        String updateSql = "update sys_org_group_cate set ornum=? where id=?";
        List<Object[]> updateList = new ArrayList<>();
        for (ZidOrnum zidOrnum : list2) {
            Object[] arr=new Object[2];
            arr[0]=zidOrnum.getOrnum()-1;
            arr[1]=zidOrnum.getId();
            updateList.add(arr);
        }
        jdbcDao.batch(updateSql, updateList);

        boolean tierChange=true;
        if ("inner".equals(po.getType()))
        {
            if(po.getDroid().equals(dragDept.getPid())){
                tierChange=false;
            }
            dragDept.setPid(po.getDroid());
            dragDept.setParent(new SysOrgGroupCate(dragDept.getPid()));
            Integer count=getCount(po.getDroid());
            dragDept.setOrnum(count+1);
        }
        else if ("before".equals(po.getType()))
        {
            SysOrgGroupCate dropDept= repo.findById(po.getDroid()).get();
            if(dropDept.getParent()!=null){
                if(dropDept.getParent().getId().equals(dragDept.getPid())){
                    tierChange=false;
                }
                dropDept.setPid(dropDept.getParent().getId());
                dragDept.setPid(dropDept.getPid());;
                dragDept.setParent(dropDept.getParent());
            }else{
                if(dragDept.getPid()==null){
                    tierChange=false;
                }
                dragDept.setPid(null);
                dragDept.setParent(null);
            }
            dragDept.setOrnum(dropDept.getOrnum());

            List<ZidOrnum> list3;
            if(StrUtil.isNotBlank(dropDept.getPid())){
                String sql3 = "select id,ornum from sys_org_group_cate where ornum>? and pid=?";
                list3 = jdbcDao.getTp().query(sql3,new Object[]{dropDept.getOrnum(),dropDept.getPid()},
                        new BeanPropertyRowMapper<>(ZidOrnum.class));
            }else{
                String sql3 = "select id,ornum from sys_org_group_cate where ornum>? and pid is null";
                list3 = jdbcDao.getTp().query(sql3,new Object[]{dropDept.getOrnum()},
                        new BeanPropertyRowMapper<>(ZidOrnum.class));
            }
            String updateSql3 = "update sys_org_group_cate set ornum=? where id=?";
            List<Object[]> updateList3 = new ArrayList<>();
            for (ZidOrnum zidOrnum : list3) {
                Object[] arr=new Object[2];
                arr[0]=zidOrnum.getOrnum()+1;
                arr[1]=zidOrnum.getId();
                updateList3.add(arr);
            }
            jdbcDao.batch(updateSql3, updateList3);
            dropDept.setOrnum(dropDept.getOrnum()+1);
            String updateSql4 = "update sys_org_group_cate set ornum=? where id=?";
            jdbcDao.update(updateSql4, dropDept.getOrnum(), dropDept.getId());
        }
        else if ("after".equals(po.getType()))
        {
            SysOrgGroupCate dropDept= repo.findById(po.getDroid()).get();
            if(dropDept.getParent()!=null){
                if(dropDept.getParent().getId().equals(dragDept.getPid())){
                    tierChange=false;
                }
                dropDept.setPid(dropDept.getParent().getId());
            }else{
                if(dragDept.getPid()==null){
                    tierChange=false;
                }
            }
            Integer count = getCount(dropDept.getPid());
            if (dragDept.getPid()!=null&&dragDept.getPid().equals(dropDept.getPid()))
            {
                dragDept.setOrnum(count);
            }
            else
            {
                dragDept.setPid(dropDept.getPid());
                if (dragDept.getPid() == null)
                {
                    dragDept.setParent(null);
                }
                else
                {
                    dragDept.setParent(new SysOrgGroupCate(dragDept.getPid()));
                }
                dragDept.setOrnum(count+1);
            }
        }
        update(dragDept,"sys_org_group_cate",tierChange);
    }


    @Autowired
    private SysOrgGroupCateRepo repo;

    @PostConstruct
    public void initDao() {
        super.setRepo(repo);
    }
}
