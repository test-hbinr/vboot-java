package vboot.core.module.sys.org.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import vboot.core.module.sys.org.root.SysOrg;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(indexes = {@Index(columnList = "usnam"),@Index(columnList = "tier"), @Index(columnList = "avtag")})
@ApiModel("组织架构用户")
public class SysOrgUser {
    @Id
    @Column(length = 32)
    @ApiModelProperty("主键ID")
    private String id;

    @Column(length = 32)
    @ApiModelProperty("名称")
    private String name;

    @Column(length = 32)
    @ApiModelProperty("昵称")
    private String ninam;

    @Transient
    @ApiModelProperty("部门ID")
    private String depid;

    @Column(length = 32)
    @ApiModelProperty("账号类型")
    private String type;//可用户区分账号用途

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "depid")
    @ApiModelProperty("部门")
    private SysOrg dept;

    @Column(length = 1000)
    @ApiModelProperty("层级")
    private String tier;

    @Column(length = 200)
    @ApiModelProperty("职务")
    private String job;

    @Column(length = 32)
    @ApiModelProperty("登录名")
    private String usnam;

    @Column(length = 64,updatable = false)
    @ApiModelProperty("密码")
    private String pacod;

    @Column(length = 64)
    @ApiModelProperty("邮箱")
    private String email;

    @Column(length = 32)
    @ApiModelProperty("手机号")
    private String monum;

    @ApiModelProperty("排序号")
    private Integer ornum;

    @ApiModelProperty("可用标记")
    private Boolean avtag;

    @Column(length = 1000)
    @ApiModelProperty("ldap层级名称")
    private String ldnam;

    @ApiModelProperty("备注")
    private String notes;

    @ApiModelProperty("通知阅读记录")
    private String relog;

    @ApiModelProperty("缓存标记")
    private Boolean catag = false;

    @Column(updatable = false)
    @ApiModelProperty("创建时间")
    private Date crtim = new Date();

    @ApiModelProperty("更新时间")
    private Date uptim;

    @Column(length = 32)
    @ApiModelProperty("标签")
    private String label;//主要用于区分组织架构分类，ABC为系统内置组织架构

    @ApiModelProperty("原始头像路径")
    private String avsrc;

    @ApiModelProperty("裁剪头像数据")
    private String avimg;

}
