package vboot.core.module.sys.coop.corp;


import org.springframework.data.jpa.repository.JpaRepository;

public interface SysCoopCorpRepo extends JpaRepository<SysCoopCorp,String> {


}
