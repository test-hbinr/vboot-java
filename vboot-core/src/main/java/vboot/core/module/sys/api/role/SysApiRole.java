package vboot.core.module.sys.api.role;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import vboot.core.common.mvc.entity.BaseMainEntity;
import vboot.core.module.sys.api.main.SysApiMain;
import vboot.core.module.sys.org.root.SysOrg;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@ApiModel("接口角色")
public class SysApiRole extends BaseMainEntity
{

    @Column(length = 32)
    @ApiModelProperty("角色类型")
    private String type;

    @Column(length = 32)
    @ApiModelProperty("角色标签")
    private String label;

    @ApiModelProperty("备注")
    private String notes;

    @ManyToMany
    @JoinTable(name = "sys_api_role_api",
            joinColumns = {@JoinColumn(name = "rid")},
            inverseJoinColumns = {@JoinColumn(name = "mid")})
    @ApiModelProperty("拥有的接口")
    private List<SysApiMain> apis  = new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "sys_api_role_org",
            joinColumns = {@JoinColumn(name = "rid")},
            inverseJoinColumns = {@JoinColumn(name = "oid")})
    @ApiModelProperty("授权的用户")
    private List<SysOrg> orgs = new ArrayList<>();

}
