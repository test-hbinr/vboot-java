package vboot.core.module.sys.coop.user;


import org.springframework.data.jpa.repository.JpaRepository;

public interface SysCoopUserRepo extends JpaRepository<SysCoopUser,String> {


}
