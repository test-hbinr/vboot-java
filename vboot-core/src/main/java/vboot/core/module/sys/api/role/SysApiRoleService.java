package vboot.core.module.sys.api.role;

import vboot.core.common.mvc.service.BaseMainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class SysApiRoleService extends BaseMainService<SysApiRole> {

    //bean注入------------------------------
    @Autowired
    private SysApiRoleRepo repo;

    @PostConstruct
    public void initDao() {
        super.setRepo(repo);
    }
}
