package vboot.core.module.sys.todo.main;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SysTodoMainRepo extends JpaRepository<SysTodoMain,String> {


}
