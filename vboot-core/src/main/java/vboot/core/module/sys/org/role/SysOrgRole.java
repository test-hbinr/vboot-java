package vboot.core.module.sys.org.role;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

//层级角色
@Data
@Entity
public class SysOrgRole {
    @Id
    @Column(length = 36)
    private String id;//主键

    @Column(length = 64)
    private String name;//角色名称

    private Integer ornum;//排序号

    private String notes;//备注

    public SysOrgRole() {
    }

    public SysOrgRole(String id) {
        this.id = id;
    }

    public SysOrgRole(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public SysOrgRole(String id, String name, Integer ornum) {
        this.id = id;
        this.name = name;
        this.ornum = ornum;
    }
}