package vboot.core.module.sys.org.group;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.Sqler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("sys/org/group")
@Api(tags = {"组织架构-群组管理"})
public class SysOrgGroupApi {

    @GetMapping
    @ApiOperation("查询群组分页")
    public R get(String name) {
        Sqler sqler = new Sqler("sys_org_group");
        sqler.addLike("t.name", name);
        sqler.addSelect("t.ornum,t.notes,t.crtim,t.uptim");
        return R.ok(service.findPageData(sqler));
    }

    @GetMapping("one/{id}")
    @ApiOperation("查询群组详情")
    public R getOne(@PathVariable String id) {
        SysOrgGroup main = service.findById(id);
        return R.ok(main);
    }

    @PostMapping
    @ApiOperation("新增群组")
    public R post(@RequestBody SysOrgGroup main) {
        return R.ok(service.insert(main));
    }

    @PutMapping
    @ApiOperation("修改群组")
    public R put(@RequestBody SysOrgGroup main) {
        return R.ok(service.update(main));
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除群组")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }

    @Autowired
    private SysOrgGroupService service;

}
