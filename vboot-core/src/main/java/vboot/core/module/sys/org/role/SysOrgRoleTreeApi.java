package vboot.core.module.sys.org.role;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.dao.Sqler;

@RestController
@RequestMapping("sys/org/rtree")
public class SysOrgRoleTreeApi {

    @GetMapping
    public R get(String name) {
        Sqler sqler = new Sqler("sys_org_role_tree");
        sqler.addLike("t.name", name);
        sqler.addSelect("t.notes");
        return R.ok(service.findPageData(sqler));
    }

    @GetMapping("one/{id}")
    public R getOne(@PathVariable String id) {
        SysOrgRoleTree main = service.findOne(id);
        return R.ok(main);
    }

    @PostMapping
    public R post(@RequestBody SysOrgRoleTree main) {
        return R.ok(service.insertx(main));
    }

    @PutMapping
    public R put(@RequestBody SysOrgRoleTree main) {
        return R.ok(service.updatex(main));
    }

    @DeleteMapping("{ids}")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }

    @GetMapping("calc")
    public R calc(String useid, String rolid) {
        return R.ok(service.calc(useid, rolid));
    }

    @GetMapping("tlist")
    public R getTlist() {
        return R.ok(service.findAll());
    }

    @GetMapping("rlist")
    public R getRlist(String treid) {
        String sql = "select id,name from sys_org_role where treid=?";
        return R.ok(jdbcDao.findMapList(sql, treid));
    }

    @Autowired
    private SysOrgRoleTreeService service;

    @Autowired
    private JdbcDao jdbcDao;

}
