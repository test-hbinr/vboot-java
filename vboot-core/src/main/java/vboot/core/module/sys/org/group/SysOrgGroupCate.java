package vboot.core.module.sys.org.group;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import vboot.core.common.mvc.entity.BaseCateEntity;
import vboot.core.module.sys.org.root.SysOrg;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@ApiModel("群组分类")
public class SysOrgGroupCate extends BaseCateEntity {

    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name = "pid")
    @ApiModelProperty("父类别")
    private SysOrgGroupCate parent;


    public SysOrgGroupCate() {

    }

    public SysOrgGroupCate(String id) {
        this.id=id;
    }
}
