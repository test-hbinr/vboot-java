package vboot.core.module.sys.coop.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import vboot.core.common.mvc.entity.BaseMainEntity;
import vboot.core.module.sys.coop.corp.SysCoopCorp;
import vboot.core.module.sys.org.root.SysOrg;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ApiModel("外部用户")
public class SysCoopUser extends BaseMainEntity {

    //区分主账号或其他用途
    @Column(length = 32)
    @ApiModelProperty("账号类型")
    private String type;

    @Transient
    @ApiModelProperty("公司ID")
    private String corid;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "corid")
    @ApiModelProperty("外部公司")
    private SysCoopCorp corp;

    @ApiModelProperty("排序号")
    private Integer ornum;

    @Column(length = 32)
    @ApiModelProperty("昵称")
    private String ninam;

    @Column(length = 32)
    @ApiModelProperty("登录名")
    private String usnam;

    @Column(length = 64,updatable = false)
    @ApiModelProperty("密码")
    private String pacod;

    @Column(length = 64)
    @ApiModelProperty("邮箱")
    private String email;

    @Column(length = 32)
    @ApiModelProperty("手机号")
    private String monum;

    @ApiModelProperty("通知阅读记录")
    private String relog;

    @ApiModelProperty("缓存标记")
    private Boolean catag = false;

    @Column(length = 1000)
    @ApiModelProperty("层级")
    private String tier;

    @ApiModelProperty("备注")
    private String notes;
}