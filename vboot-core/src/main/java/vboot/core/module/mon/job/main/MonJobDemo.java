package vboot.core.module.mon.job.main;

import cn.hutool.core.date.DateUtil;
import vboot.core.module.mon.job.root.IJob;
import vboot.core.module.mon.job.root.IJobGroup;
import org.springframework.stereotype.Component;

@IJobGroup
@Component
public class MonJobDemo
{
    private static boolean usingFlag = false;

    @IJob(code="demo1",cron = "0/10 * 0-23 * * ?", name = "10秒执行一次的DEMO1")
    public void demo1()
    {
        System.out.println("定时任务-demo1:" + DateUtil.now());
    }

    @IJob(code="demo2",cron = "0/20 * 0-23 * * ?", name = "20秒执行一次的DEMO2")
    public void demo2()
    {
        if (!usingFlag)
        {
            usingFlag = true;
            try
            {
                System.out.println("定时任务-demo2:" + DateUtil.now());
            } finally
            {
                usingFlag = false;
            }
        }

    }
}