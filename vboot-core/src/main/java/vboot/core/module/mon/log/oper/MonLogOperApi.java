package vboot.core.module.mon.log.oper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.dao.Sqler;

@RestController
@RequiredArgsConstructor
@RequestMapping("mon/log/oper")
@Api(tags = {"操作日志"})
public class MonLogOperApi {

    @GetMapping
    @ApiOperation("查询操作日志分页")
    public R get() {
        Sqler sqler = new Sqler("t.id,t.name,t.useid,t.method,t.param,t.type,t.ip,t.addre", "mon_log_oper");
        sqler.addSelect("t.agbro,t.ageos,t.time,t.crtim");
        sqler.addOrder("t.crtim desc");
        sqler.addLeftJoin("u.name as usena,u.usnam","sys_org_user u","u.id=t.useid");
        return R.ok(jdbcDao.findPageData(sqler));
    }

    @GetMapping("one/{id}")
    @ApiOperation("查询操作日志详情")
    public R getOne(@PathVariable String id) {
        MonLogOper main = service.findOne(id);
        return R.ok(main);
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除错误日志")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }

    @DeleteMapping(value = "all")
    @ApiOperation("清空错误日志")
    public R delAllInfoLog() {
        service.delAll();
        return R.ok();
    }

    @Autowired
    private JdbcDao jdbcDao;

    private final MonLogOperService service;

}
