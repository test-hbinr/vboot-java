package vboot.core.module.mon.log.login;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Data
@ApiModel("登录日志")
public class MonLogLogin {
    @Id
    @Column(length = 32)
    @ApiModelProperty("主键ID")
    private String id;

    @Column(length = 32)
    @ApiModelProperty("用户名称")
    private String name;

    @Column(length = 32)
    @ApiModelProperty("登录账号")
    private String usnam;

    @Column(length = 32)
    @ApiModelProperty("登录ip")
    private String ip;

    @Column(updatable = false)
    @ApiModelProperty("登录时间")
    private Date crtim=new Date();

    @Column(length = 32)
    @ApiModelProperty("登录地点")
    private String addre;

    @Column(length = 64)
    @ApiModelProperty("session")
    private String usession;//取消了session!!!

    @Column(length = 64)
    @ApiModelProperty("在线KEY")
    private String onkey;

    @Column(length = 32)
    @ApiModelProperty("操作系统")
    private String ageos;

    @Column(length = 32)
    @ApiModelProperty("浏览器")
    private String agbro;

    @Column(length = 512)
    @ApiModelProperty("客户端详情")
    private String agdet;

}
