package vboot.app.my.demo.main;

import org.springframework.data.jpa.repository.JpaRepository;

//DEMO主数据JPA仓储
public interface MyDemoMainRepo extends JpaRepository<MyDemoMain,String> {


}

